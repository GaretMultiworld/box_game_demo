﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public string type;
    Animator anim;
    Rigidbody2D rigid;
    AudioSource sfx;
    BoxCollider2D coll;

    public void Initialize()
    {
        anim = gameObject.GetComponent<Animator>();
        rigid = gameObject.GetComponent<Rigidbody2D>();
        sfx = gameObject.GetComponent<AudioSource>();
        coll = gameObject.GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameManager.instance.itemTouched(type);
            CrashAnim();
        }
        else
        {
            if (type == "box")
            {
                GameManager.instance.addScore(1);
            }
            CrashAnim();
        }
    }

    public void CrashAnim()
    {
        coll.enabled = false;
        rigid.simulated = false;
        anim.SetTrigger("Crash");
    }

    void Inactive ()
    {
        gameObject.SetActive(false);
    }

    void SoundEffect ()
    {
        sfx.Play();
    }

    public void Drop()
    {
        coll.enabled = true;
        rigid.simulated = true;
    }

}
