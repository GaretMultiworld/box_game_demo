﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesLoader : MonoBehaviour {

    public class InstanceOptions
    {
        public bool cacheable = true;
        public string name = "";
        public Vector3 position = Vector3.zero;
        public Transform parent = null;
    }

    public static Dictionary<string, Object> resources = new Dictionary<string, Object>();
    public static Dictionary<string, int> instancesCount = new Dictionary<string, int>();

    public static Object Cache(string path)
    {
        Object resource;
        if (!resources.ContainsKey(path))
        {
            resource = Resources.Load(path, typeof(GameObject));
            resources.Add(path, resource);
            instancesCount.Add(path, 0);
        }
        else
        {
            resource = resources[path];
        }
        instancesCount[path]++;
        return resource;
    }

    public static GameObject Instantiate(string path, InstanceOptions options)
    {
        bool assignName = options.name.Equals("");
        string name;
        if (assignName)
        {
            name = path.Replace('/', '_');
        }
        else
        {
            name = options.name;
        }
        Object resource;
        if (options.cacheable || resources.ContainsKey(path))
        {
            resource = Cache(path);
            if (assignName)
            {
                name += " (" + instancesCount[path] + ")";
            }
        }
        else
        {
            resource = Resources.Load(path, typeof(GameObject));
        }
        GameObject instance = Object.Instantiate(resource) as GameObject;
        instance.name = name;
        if (options.parent!=null)
        {
            instance.transform.SetParent( options.parent);
        }
        instance.transform.localPosition = options.position;
        return instance;
    }

}
