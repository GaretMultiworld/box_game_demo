﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour {

    public Sprite sprite01;
    public Sprite sprite02;
    public Sprite sprite03;
    public Sprite sprite04;
    public int spriteType;
    public SpriteRenderer rend;

    public void InitializeBonus () {
        switch (Random.Range(1,5))
        {
            case 1:
                Debug.Log(1);
                rend.sprite = sprite01;
                break;
            case 2:
                Debug.Log(2);
                rend.sprite = sprite02;
                break;
            case 3:
                Debug.Log(3);
                rend.sprite = sprite03;
                break;
            case 4:
                Debug.Log(4);
                rend.sprite = sprite04;
                break;
        }
	}
	
}
