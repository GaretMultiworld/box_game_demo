﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDropper : MonoBehaviour {

    int currentWave;
    bool dropperEnabled;

    int cachedBoxes;
    int cachedBombs;
    int cachedBonuses;

    GameObject[] boxes;
    GameObject[] bombs;
    GameObject[] bonuses;

    bool[] pausedBoxes;
    bool[] pausedBombs;
    bool[] pausedBonuses;

    int nextBox;
    int nextBomb;
    int nextBonus;

    float leftLimit;
    float rightLimit;

    float waveTimeLength;
    float boxDropChance;
    float bombDropChance;
    float timeBetweenWaves;
    float dropsPerWaveLevel;

    float timer;
    float waveTimeLeft;
    float dropTimer;
    bool inWave;
    float intervalBetweenDrops;
    int dropsRemaining;
    float yDropPos;
    float zDropPos;

    float minBoxGravity;
    float maxBoxGravity;
    float minBombGravity;
    float maxBombGravity;
    float minBonusGravity;
    float maxBonusGravity;
    HUD hud;

    public void CustomStart()
    {
        hud = GameManager.instance.hud;
        yDropPos = transform.position.y;
        zDropPos = transform.position.z;
        Bounds bounds = CameraExtensions.OrthographicBounds(GameManager.instance.getCamera());
        leftLimit = bounds.min.x + 2;
        rightLimit = bounds.max.x - 2;
        ResourcesLoader.InstanceOptions options = new ResourcesLoader.InstanceOptions();
        cachedBoxes = GameManager.instance.config.cachedBoxes;
        cachedBombs = GameManager.instance.config.cachedBombs;
        cachedBonuses = GameManager.instance.config.cachedBonuses;
        waveTimeLength = GameManager.instance.config.waveTimeLength;
        boxDropChance = GameManager.instance.config.boxDropChance;
        bombDropChance = GameManager.instance.config.bombDropChance;
        timeBetweenWaves = GameManager.instance.config.timeBetweenWaves;
        dropsPerWaveLevel = GameManager.instance.config.dropsPerWaveLevel;
        minBoxGravity = GameManager.instance.config.minBoxGravity;
        maxBoxGravity = GameManager.instance.config.maxBoxGravity;
        minBombGravity = GameManager.instance.config.minBombGravity;
        maxBombGravity = GameManager.instance.config.maxBombGravity;
        minBonusGravity = GameManager.instance.config.minBonusGravity;
        maxBonusGravity = GameManager.instance.config.maxBonusGravity;
        nextBox =0;
        nextBomb=0;
        nextBonus=0;
        pausedBoxes = new bool[cachedBoxes];
        boxes = new GameObject[cachedBoxes];
        for (int pos = 0; pos < boxes.Length; pos++)
        {
            boxes[pos] = ResourcesLoader.Instantiate(GameManager.instance.config.GetBoxPrefab(), options);
            boxes[pos].GetComponent<Item>().Initialize();
            boxes[pos].SetActive(false);
        }
        pausedBombs = new bool[cachedBombs];
        bombs = new GameObject[cachedBombs];
        for (int pos = 0; pos < bombs.Length; pos++)
        {
            bombs[pos] = ResourcesLoader.Instantiate(GameManager.instance.config.GetBombPrefab(), options);
            bombs[pos].GetComponent<Item>().Initialize();
            bombs[pos].SetActive(false);
        }
        pausedBonuses = new bool[cachedBonuses];
        bonuses = new GameObject[cachedBonuses];
        for (int pos = 0; pos < bonuses.Length; pos++)
        {
            bonuses[pos] = ResourcesLoader.Instantiate(GameManager.instance.config.GetBonusPrefab(), options);
            bonuses[pos].GetComponent<Item>().Initialize();
            bonuses[pos].SetActive(false);
        }
    }

    public void StartGame()
    {
        currentWave = 1;
        SetEnabled(true);
        timer = 0;
        waveTimeLeft = waveTimeLength;
        hud.SetTimeLeft(waveTimeLeft);
        dropTimer = 0;
        inWave = false;
        dropsRemaining = ((int)System.Math.Floor(dropsPerWaveLevel)) * currentWave;
        intervalBetweenDrops = waveTimeLength / dropsRemaining;
        for (int pos = 0; pos < boxes.Length; pos++)
        {
            boxes[pos].SetActive(false);
        }
        for (int pos = 0; pos < bombs.Length; pos++)
        {
            bombs[pos].SetActive(false);
        }
        for (int pos = 0; pos < bonuses.Length; pos++)
        {
            bonuses[pos].SetActive(false);
        }
    }

    public void EndGame()
    {
        SetEnabled(false);
    }

    public void SetEnabled(bool enabled)
    {
        dropperEnabled = enabled;
        gameObject.SetActive(enabled);
        for (int pos = 0; pos < boxes.Length; pos++)
        {
            if (boxes[pos].activeSelf && !enabled)
            {
                pausedBoxes[pos] = true;
                boxes[pos].SetActive(false);
            }
            else if(pausedBoxes[pos] && enabled)
            {
                pausedBoxes[pos] = false;
                boxes[pos].SetActive(true);
            }
        }
        for (int pos = 0; pos < bombs.Length; pos++)
        {
            if (bombs[pos].activeSelf && !enabled)
            {
                pausedBombs[pos] = true;
                bombs[pos].SetActive(false);
            }
            else if (pausedBombs[pos] && enabled)
            {
                pausedBombs[pos] = false;
                bombs[pos].SetActive(true);
            }
        }
        for (int pos = 0; pos < bonuses.Length; pos++)
        {
            if (bonuses[pos].activeSelf && !enabled)
            {
                pausedBonuses[pos] = true;
                bonuses[pos].SetActive(false);
            }
            else if (pausedBonuses[pos] && enabled)
            {
                pausedBonuses[pos] = false;
                bonuses[pos].SetActive(true);
            }
        }
    }

    public void CustomUpdate()
    {
        if (!dropperEnabled)
        {
            return;
        }
        timer += Time.deltaTime;
        if (!inWave)
        {
            if(timer > timeBetweenWaves)
            {
                waveTimeLeft = waveTimeLength;
                hud.SetTimeLeft(waveTimeLeft);
                timer = 0;
                inWave = true;
            }
        }
        else
        {
            waveTimeLeft -= Time.deltaTime;
            hud.SetTimeLeft(waveTimeLeft);
            dropTimer += Time.deltaTime;
            while (dropTimer > intervalBetweenDrops)
            {
                dropTimer -= intervalBetweenDrops;
                DropItem();
            }
            if (timer > waveTimeLength)
            {
                timer = 0;
                inWave = false;
                currentWave++;
                GameManager.instance.WaveCompleted(currentWave);
                for (int pos = 0; pos < boxes.Length; pos++)
                {
                    boxes[pos].SetActive(false);
                    pausedBoxes[pos] = false;
                }
                for (int pos = 0; pos < bombs.Length; pos++)
                {
                    bombs[pos].SetActive(false);
                    pausedBombs[pos] = false;
                }
                for (int pos = 0; pos < bonuses.Length; pos++)
                {
                    pausedBonuses[pos] = false;
                    bonuses[pos].SetActive(false);
                }
                dropsRemaining = ((int)System.Math.Floor(dropsPerWaveLevel)) * currentWave;
                intervalBetweenDrops = waveTimeLength / dropsRemaining;
            }
        }
    }

    void DropItem()
    {
        Vector3 newPos = new Vector3(Random.Range(leftLimit, rightLimit), yDropPos, zDropPos);
        if (Random.Range(0f,1f)<boxDropChance)
        {
            boxes[nextBox].GetComponent<Rigidbody2D>().gravityScale = Random.Range(minBoxGravity, maxBoxGravity);
            boxes[nextBox].SetActive(true);
            boxes[nextBox].GetComponent<Item>().Drop();
            boxes[nextBox].transform.position = newPos;
            nextBox++;
            if (nextBox>=boxes.Length)
            {
                nextBox = 0;
            }
        }
        else if (Random.Range(0f, 1f-boxDropChance) < bombDropChance)
        {
            bombs[nextBomb].GetComponent<Rigidbody2D>().gravityScale = Random.Range(minBombGravity, maxBombGravity);
            bombs[nextBomb].SetActive(true);
            bombs[nextBomb].GetComponent<Item>().Drop();
            bombs[nextBomb].transform.position = newPos;
            nextBomb++;
            if (nextBomb >= bombs.Length)
            {
                nextBomb = 0;
            }
        }
        else
        {
            bonuses[nextBonus].GetComponent<Rigidbody2D>().gravityScale = Random.Range(minBonusGravity, maxBonusGravity);
            bonuses[nextBonus].SetActive(true);
            bonuses[nextBonus].GetComponent<Item>().Drop();
            bonuses[nextBonus].transform.position = newPos;
            nextBonus++;
            if (nextBonus >= bonuses.Length)
            {
                nextBonus = 0;
            }
        }
    }

    public void Bomb()
    {
        for (int pos = 0; pos < boxes.Length; pos++)
        {
            if (boxes[pos].activeSelf)
            {
                GameManager.instance.addScore(1);
                boxes[pos].GetComponent<Item>().CrashAnim();
            }
        }
    }

}
