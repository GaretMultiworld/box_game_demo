﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    bool playerEnabled = true;

    string horizontalAxis;

    float moveSpeed;
    float leftLimit = -120;
    float rightLimit = 120;

    public Animator anim;

    public bool leftButton=false;
    public bool rightButton = false;

    public void SetEnabled(bool enabled)
    {
        playerEnabled = enabled;
        gameObject.SetActive(enabled);
        anim.SetBool("isWalking", false);
    }

    public void StartGame()
    {
        SetEnabled(true);
    }

    public void EndGame()
    {
        SetEnabled(false);
    }

    public void CustomStart()
    {
        horizontalAxis = GameManager.instance.config.horizontalAxis;
        moveSpeed = GameManager.instance.config.moveSpeed;
        Bounds bounds=CameraExtensions.OrthographicBounds(GameManager.instance.getCamera());
        leftLimit = bounds.min.x+1;
        rightLimit= bounds.max.x-1;
    }

    public void CustomUpdate()
    {
        if (!playerEnabled)
        {
            return;
        }
        float translation;
        int axis = 0;
        if (leftButton&&!rightButton)
        {
            axis = -1;
        }
        else if(rightButton&&!leftButton)
        {
            axis = 1;
        }
        translation = axis * moveSpeed * Time.deltaTime;
        /*if (Input.touchCount > 0)
        {
            int axis = 0;
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                if (touch.position.x < GameManager.instance.cameraSizeXHalf)
                {
                    axis = -1;
                }
                else
                {
                    axis = 1;
                }
            }
            translation = axis * moveSpeed * Time.deltaTime;
        }
        else
        {
            translation = Input.GetAxis(horizontalAxis) * moveSpeed * Time.deltaTime;
        }*/
        transform.Translate(translation, 0, 0);
        if (translation < 0)
        {
            transform.localScale = new Vector3(1.5f, 1.5f, 1);
        }
        else if (translation > 0)
        {
            transform.localScale = new Vector3(-1.5f, 1.5f, 1);
        }
        anim.SetBool("isWalking", translation != 0);
        if (transform.position.x < leftLimit)
        {
            transform.position = new Vector3(leftLimit, transform.position.y, transform.position.z);
        }
        if (transform.position.x > rightLimit)
        {
            transform.position = new Vector3(rightLimit, transform.position.y, transform.position.z);
        }
    }

}
