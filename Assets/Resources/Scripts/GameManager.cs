﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public Config config;

    public Player player;
    GameObject startScreenGo;
    StartScreen startScreen;
    EndScreen endScreen;
    GameObject endScreenGo;
    GameObject hudGo;
    public HUD hud;
    EndScreen hudEndScreen;
    Camera loadedCamera;
    ItemDropper itemDropper;
    float score=0;
    float time = 0;
    bool paused = false;
    public float cameraSizeX;
    public float cameraSizeXHalf;
    public float cameraSizeY;

    int lifes;

    public void itemTouched(string type)
    {
        switch (type)
        {
            case "box":
                SetLifes(lifes - 1);
                player.anim.SetTrigger("isHurt");
                if (lifes == 0)
                {
                    EndGame();
                }
                break;
            case "bomb":
                itemDropper.Bomb();
                break;
            case "bonus":
                addScore(config.bonusScore);
                break;
        }
    }

    public void addScore(float amount)
    {
        score += amount;
        hud.SetScore(score);
    }

    public Camera getCamera()
    {
        return loadedCamera;
    }

    void Awake () {
        instance = this;
        ResourcesLoader.InstanceOptions options = new ResourcesLoader.InstanceOptions();
        options.cacheable = false;
        startScreenGo = ResourcesLoader.Instantiate(config.startScreenPrefab, options);
        startScreen = startScreenGo.GetComponent<StartScreen>();
        endScreenGo = ResourcesLoader.Instantiate(config.endScreenPrefab, options);
        endScreen =endScreenGo.GetComponent<EndScreen>();
        endScreenGo.SetActive(false);
        hudGo = ResourcesLoader.Instantiate(config.hudPrefab, options);
        hud = hudGo.GetComponent<HUD>();
        hud.CutomStart();
        hudEndScreen = hudGo.GetComponent<EndScreen>();
        hudGo.SetActive(false);
        options.position = new Vector3(4.62f, -13.7f, 0f);
        player = ResourcesLoader.Instantiate(config.playerPrefab, options).GetComponent<Player>();
        options.position = new Vector3(0, 2.4f, -22.4f);
        GameObject cameraGO = ResourcesLoader.Instantiate(config.cameraPrefab, options);
        loadedCamera = cameraGO.GetComponent<Camera>();
        cameraSizeY= 2f * loadedCamera.orthographicSize* -cameraGO.transform.position.z;
        cameraSizeX= cameraSizeY * loadedCamera.aspect;
        cameraSizeXHalf = cameraSizeX / 2;
        options.position = new Vector3(6.563848f, 7.883767f, -0.3359375f);
        ResourcesLoader.Instantiate(config.environmentPrefab, options);
        options.position = new Vector3(6.563848f, 29.6f, -0.3359375f);
        itemDropper=ResourcesLoader.Instantiate(config.itemDropperPrefab, options).GetComponent<ItemDropper>();
        itemDropper.CustomStart();
        itemDropper.SetEnabled(false);
        player.SetEnabled(false);
    }

    private void Start()
    {
        endScreen.CutomStart();
        hudEndScreen.CutomStart();
        startScreen.CutomStart();
        player.CustomStart();
        itemDropper.CustomStart();
    }

    private void Update()
    {
        player.CustomUpdate();
        itemDropper.CustomUpdate();
        if (!paused)
        {
            time += Time.deltaTime;
        }
    }

    public void StartGame()
    {
        SetLifes(config.lifes);
        paused = false;
        startScreenGo.SetActive(false);
        endScreenGo.SetActive(false);
        hudGo.SetActive(true);
        player.StartGame();
        itemDropper.StartGame();
        score = 0;
        time = 0;
        hud.SetScore(score);
        hud.ResetWave();
    }

    void SetLifes(int newLifes)
    {
        hud.SetLifes(newLifes);
        lifes = newLifes;
    }

    public void EndGame()
    {
        paused = true;
        player.EndGame();
        itemDropper.EndGame();
        hudGo.SetActive(false);
        endScreen.SetScore(score);
        endScreen.SetTime(time);
        endScreenGo.SetActive(true);
    }

    public void WaveCompleted(int wave)
    {
        paused = true;
        player.SetEnabled(false);
        itemDropper.SetEnabled(false);
        hudEndScreen.SetScore(score);
        hudEndScreen.SetTime(time);
        hud.SetCurrentWave(wave);
    }

    public void ResumeGame()
    {
        paused = false;
        player.SetEnabled(true);
        itemDropper.SetEnabled(true);
        hudEndScreen.SetScore(0);
        hudEndScreen.SetTime(0);
        hud.waveCompleteAnim.SetTrigger("continue");
    }

    public void MainMenu()
    {
        paused = true;
        hudGo.SetActive(false);
        player.SetEnabled(false);
        itemDropper.SetEnabled(false);
        startScreenGo.SetActive(true);
    }

    public void TogglePause()
    {
        paused = !paused;
        player.SetEnabled(!paused);
        itemDropper.SetEnabled(!paused);
    }

    public void HUD()
    {
        hud.waveCompleteAnim.SetTrigger("continue");
    }
}
