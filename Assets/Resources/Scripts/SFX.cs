﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour {

    public AudioSource sfx;

    void PlaySFX()
    {
        sfx.Play();
    }
}
