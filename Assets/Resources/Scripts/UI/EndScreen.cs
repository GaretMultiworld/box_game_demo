﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    public Button restartButton;
    public Button resumeButton;
    public Button exitButton;
    public Text scoreText;
    public Text timeText;

    public void CutomStart()
    {
        Button button = restartButton.GetComponent<Button>();
        button.onClick.AddListener(RestartButtonClick);
        if (resumeButton != null)
        {
            Button button1 = resumeButton.GetComponent<Button>();
            button1.onClick.AddListener(ResumeButtonClick);
        }
        Button button2 = exitButton.GetComponent<Button>();
        button2.onClick.AddListener(ExitButtonClick);
    }

    public void SetScore(float score)
    {
        scoreText.text = score.ToString("#");
    }

    public void SetTime(float time)
    {
        if (time == 0)
        {
            timeText.text = "";
            return;
        }
        int seconds = (int)Mathf.Floor(time);
        int minutes = seconds / 60;
        int hours = 0;
        string text;
        if (minutes > 0)
        {
            seconds -= minutes * 60;
            hours = minutes / 60;
            if (hours>0)
            {
                minutes -= hours * 60;
                text = hours+":"+minutes + ":" + seconds;
            }
            else
            {
                text = minutes+":"+seconds;
            }
        }
        else
        {
            text = seconds.ToString();
        }
        timeText.text = text;
    }

    void RestartButtonClick()
    {
        GameManager.instance.HUD();
        GameManager.instance.StartGame();        
    }

    void ExitButtonClick()
    {
        GameManager.instance.MainMenu();
    }

    void ResumeButtonClick()
    {
        GameManager.instance.ResumeGame();
    }

}
