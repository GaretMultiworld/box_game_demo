﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{

    public Text scoreText;
    public Text timeLeftText;
    public Text lifesText;
    public Text currentWaveText;
    public Animator waveCompleteAnim;
    public Toggle pauseButton;
    public Button restartButton;
    public Button exitButton;

    public void LeftPress()
    {
        GameManager.instance.player.leftButton = true;
    }

    public void LeftRelease()
    {
        GameManager.instance.player.leftButton = false;
    }

    public void RightPress()
    {
        GameManager.instance.player.rightButton = true;
    }

    public void RightRelease()
    {
        GameManager.instance.player.rightButton = false;
    }

    public void CutomStart()
    {
        Toggle button = pauseButton.GetComponent<Toggle>();
        button.onValueChanged.AddListener(delegate {
            PauseButtonClick();
        });
        Button button2 = restartButton.GetComponent<Button>();
        button2.onClick.AddListener(RestartButtonClick);
        Button button3 = exitButton.GetComponent<Button>();
        button3.onClick.AddListener(ExitButtonClick);
    }

    public void SetScore(float score)
    {
        scoreText.text = (score == 0 ? "0" : score.ToString("#"));
    }

    public void SetCurrentWave(int wave)
    {
        waveCompleteAnim.SetTrigger("show");
        currentWaveText.text = wave.ToString();
    }

    public void ResetWave()
    {
        currentWaveText.text = "1";
    }

    public void SetLifes(int lifes)
    {
        lifesText.text = (lifes == 0 ? "0" : lifes.ToString());
    }

    void PauseButtonClick()
    {
        GameManager.instance.TogglePause();
    }

    void RestartButtonClick()
    {
        GameManager.instance.StartGame();
    }

    void ExitButtonClick()
    {
        GameManager.instance.MainMenu();
    }

    public void SetTimeLeft(float time)
    {
        if (time == 0)
        {
            timeLeftText.text = "";
            return;
        }
        int seconds = (int)Mathf.Floor(time);
        int minutes = seconds / 60;
        int hours = 0;
        string text;
        if (minutes > 0)
        {
            seconds -= minutes * 60;
            hours = minutes / 60;
            if (hours > 0)
            {
                minutes -= hours * 60;
                text = hours + ":" + minutes + ":" + seconds;
            }
            else
            {
                text = minutes + ":" + seconds;
            }
        }
        else
        {
            text = seconds.ToString();
        }
        timeLeftText.text = text;
    }

}
