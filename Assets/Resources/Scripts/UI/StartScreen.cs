﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour {

    public Button startButton;
    public AudioSource sfx;

    public void CutomStart () {
        Button button = startButton.GetComponent<Button>();
        button.onClick.AddListener(StartButtonClick);
    }

    void StartButtonClick()
    {
        StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        sfx.Play();
        yield return new WaitForSeconds(1);
        GameManager.instance.StartGame();
    }

}
