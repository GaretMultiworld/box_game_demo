﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour {

    public string playerPrefab;
    public string cameraPrefab;
    public string environmentPrefab;
    public string itemDropperPrefab;
    public string []boxPrefabs;
    public string []bombPrefabs;
    public string []bonusPrefabs;

    public string startScreenPrefab;
    public string endScreenPrefab;
    public string hudPrefab;

    public string horizontalAxis;
    public float moveSpeed;

    public int cachedBoxes;
    public int cachedBombs;
    public int cachedBonuses;

    public float waveTimeLength;
    public float dropsPerWaveLevel;
    public float boxDropChance;
    public float bombDropChance;
    public float timeBetweenWaves;
    public float minBoxGravity;
    public float maxBoxGravity;
    public float minBombGravity;
    public float maxBombGravity;
    public float minBonusGravity;
    public float maxBonusGravity;

    public int bonusScore;

    public int lifes;

    public string GetBoxPrefab()
    {
        return boxPrefabs[Random.Range(0,boxPrefabs.Length)];
    }

    public string GetBombPrefab()
    {
        return bombPrefabs[Random.Range(0, bombPrefabs.Length)];
    }

    public string GetBonusPrefab()
    {
        return bonusPrefabs[Random.Range(0, bonusPrefabs.Length)];
    }

}
